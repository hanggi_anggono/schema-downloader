const fetch = require("node-fetch");
const { introspectionQuery } = require("graphql");
const fs = require("fs");
const { uncrunch } = require("graphql-crunch");


fetch("https://kjjpup3by7.execute-api.ap-southeast-1.amazonaws.com/staging/graphql-introspect?crunch=2", {
  method: "POST",
  headers: { "Content-Type": "application/json" },
  body: JSON.stringify({ query: introspectionQuery })
})
  .then(res => res.json())
  .then((res) => {
    fs.writeFileSync("schema-staging.json", JSON.stringify(
      uncrunch(res.data), null, 2)
    );
  });
